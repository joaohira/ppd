/*
    This program solves Laplace's equation on a regular 2D grid using simple Jacobi iteration.

    The stencil calculation stops when  iter > ITER_MAX
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>

#define ITER_MAX 3000 // number of maximum iterations

double **grid;

double **new_grid;

int size;

void allocate_memory()
{
    grid = (double **)malloc(size * sizeof(double *));
    new_grid = (double **)malloc(size * sizeof(double *));

    for (int i = 0; i < size; i++)
    {
        grid[i] = (double *)malloc(size * sizeof(double));
        new_grid[i] = (double *)malloc(size * sizeof(double));
    }
}

void initialize_grid()
{
    int linf = size / 2;
    int lsup = linf + size / 10;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (i >= linf && i < lsup && j >= linf && j < lsup)
                grid[i][j] = 100.0;
            else
                grid[i][j] = 0.0;
            new_grid[i][j] = 0.0;
        }
    }
}

void save_grid()
{
    char file_name[30];
    sprintf(file_name, "grid_laplace.txt");

    FILE *file;
    file = fopen(file_name, "w");

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            fprintf(file, "%lf ", grid[i][j]);
        }
        fprintf(file, "\n");
    }

    fclose(file);
}

void compute_grid()
{
    int iter = 0;

    while (iter <= ITER_MAX)
    {
        #pragma omp parallel
        {
            // calculates the Laplace equation to determine each cell's next value
            // kernel 1

            #pragma omp for collapse(2)
            for (int i = 1; i < size - 1; i++)
            {
                for (int j = 1; j < size - 1; j++)
                {
                    new_grid[i][j] = 0.25 * (grid[i][j + 1] + grid[i][j - 1] +
                                             grid[i - 1][j] + grid[i + 1][j]);
                }
            }

            // copy the next values into the working array for the next iteration
            // kernel 2
            #pragma omp for collapse(2)
            for (int i = 1; i < size - 1; i++)
            {
                for (int j = 1; j < size - 1; j++)
                {
                    grid[i][j] = new_grid[i][j];
                }
            }
        }
        
        iter++;
    }
}

int main(int argc, char *argv[])
{

    if (argc != 2)
    {
        printf("Usage: ./laplace_seq N\n");
        printf("N: The size of each side of the domain (grid)\n");
        exit(-1);
    }

    struct timeval time_start;
    struct timeval time_end;

    size = atoi(argv[1]);

    allocate_memory();

    initialize_grid();

    printf("Jacobi relaxation calculation: %d x %d grid\n", size, size);

    gettimeofday(&time_start, NULL);

    compute_grid();

    gettimeofday(&time_end, NULL);

    double exec_time = (double)(time_end.tv_sec - time_start.tv_sec) +
                       (double)(time_end.tv_usec - time_start.tv_usec) / 1000000.0;

    // save_grid();

    printf("Kernel executed in %lf seconds with %d iterations\n", exec_time, ITER_MAX);

    return 0;
}
