#!/bin/bash
#SBATCH -J laplace_omp
#SBATCH -p fast
#SBATCH -n 1
#SBATCH -t 01:30:00
#SBATCH --cpus-per-task=40
#SBATCH --output=%x.%j.out
#SBATCH --error=%x.%j.err

scratch=/scratch/u759055
size=1000

mkdir $scratch
cp container.sif $scratch
cd $scratch

printf "\n*** laplace_pth - OpenMP ***\n"
for n_threads in 1 2 5 10 20 40
do
    printf "\nn_threads = $n_threads\n\t"
    export OMP_NUM_THREADS=$n_threads
    singularity run container.sif laplace_omp $size
done
