#!/bin/bash
#SBATCH -J laplace
#SBATCH -p fast
#SBATCH -n 1
#SBATCH -t 01:30:00
#SBATCH --cpus-per-task=40
#SBATCH --output=%x.%j.out
#SBATCH --error=%x.%j.err

size=1000

printf "\n*** laplace_pth - Pthreads ***\n"
for n_threads in 1 2 5 10 20 40
do
    printf "\nn_threads = $n_threads\n\t"
    singularity run container.sif laplace_pth $size $n_threads
done
