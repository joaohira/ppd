#!/bin/bash
#SBATCH -J Trabalho_final           # Job name
#SBATCH -p fast                     # Job partition
#SBATCH -n 1                        # Number of processes
#SBATCH -t 00:05:00                 # Run time (hh:mm:ss)
#SBATCH --cpus-per-task=128          # Number of CPUs per process
#SBATCH --output=%x.%j.out          # Name of stdout output file - %j expands to jobId and %x to jobName
#SBATCH --error=%x.%j.err           # Name of stderr output file

N1=500
N2=500
TIME=5000

printf "\n*** SEQUENCIAL ***\n"
srun singularity run container.sif wave_seq $N1 $N2 $TIME

printf "\n*** OPENMP ***\n"
for n_threads in 1 2 5 10 20 40
do
    srun singularity run container.sif wave_omp $N1 $N2 $TIME $n_threads
done