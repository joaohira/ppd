import imageio.v3 as iio
from glob import glob
from natsort import natsorted
import numpy as np
from pygifsicle import optimize

filenames = glob('./plots/frames/*.png')

images = []

for filename in natsorted(filenames):
    images.append(iio.imread(filename))

frames = np.stack(images)

iio.imwrite('plot.gif', frames, duration=1)
optimize('plot.gif', 'optimized.gif')