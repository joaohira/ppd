#!/bin/bash

size=100000000

printf "\n*** vecadd_mpi - MPI ***\n"
for n_threads in {1..8}
do
    printf "\nn_threads = $n_threads\n\t"
    mpirun -np $n_threads ./app/vecadd_mpi $size 
done