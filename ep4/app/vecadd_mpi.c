#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <mpi.h>

#define TOL 0.0000001

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        printf("Usage: ./vecadd_seq N\n");
        printf("N: Size of the vectors\n");
        exit(-1);
    }

    double t1, t2;

    float *a, *b, *c, *c_expected;
    float *subvector_a, *subvector_b, *subvector_c;

    int array_size = atoi(argv[1]);
    int error_count = 0;

    int rank, num_procs;
    int array_size_per_proc, remainder;

    int *send_counts, *displacement;
    int recv_count;

    MPI_Init(NULL, NULL);

    // get the number of processes and the rank of the current process
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // calculate the size of the subvector for each process
    array_size_per_proc = array_size / num_procs;
    // remainder of the division for the last process
    remainder = array_size % num_procs;

    // arrays to store the number of elements to send to each process
    // and the displacement of elements
    send_counts = (int *)malloc(num_procs * sizeof(int));
    displacement = (int *)malloc(num_procs * sizeof(int));

    if (rank == 0)
    {
        a = (float *)malloc(array_size * sizeof(float));
        b = (float *)malloc(array_size * sizeof(float));
        c = (float *)malloc(array_size * sizeof(float));
        c_expected = (float *)malloc(array_size * sizeof(float));

        // fill the arrays
        for (int i = 0; i < array_size; i++)
        {
            a[i] = (float)i;
            b[i] = 2.0 * (float)i;
            c[i] = 0.0;
            c_expected[i] = (float)i + 2 * (float)i;
        }

        for (int i = 0; i < num_procs; i++)
        {
            // number of elements to send to each process
            send_counts[i] = array_size_per_proc;
            // displacement of elements for each process
            displacement[i] = i * array_size_per_proc;

            // last process gets the remainder
            if (i == num_procs - 1)
            {
                send_counts[i] += remainder;
            }
        }
    }

    // broadcast the send_counts and displacement arrays to all processes
    MPI_Bcast(send_counts, num_procs, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(displacement, num_procs, MPI_INT, 0, MPI_COMM_WORLD);

    // each process allocates memory for its subvector
    recv_count = send_counts[rank];
    subvector_a = (float *)malloc(sizeof(float) * recv_count);
    subvector_b = (float *)malloc(sizeof(float) * recv_count);
    subvector_c = (float *)malloc(sizeof(float) * recv_count);

    // scatter the vectors to all processes
    MPI_Scatterv(a, send_counts, displacement, MPI_FLOAT, subvector_a, recv_count, MPI_FLOAT, 0, MPI_COMM_WORLD);
    MPI_Scatterv(b, send_counts, displacement, MPI_FLOAT, subvector_b, recv_count, MPI_FLOAT, 0, MPI_COMM_WORLD);

    // synchronize all processes before starting the timer
    MPI_Barrier(MPI_COMM_WORLD);

    // get the start time
    t1 = MPI_Wtime();

    // each process adds its subvectors
    for (int i = 0; i < recv_count; i++)
    {
        subvector_c[i] = subvector_a[i] + subvector_b[i];
    }

    // gather the results back to the root process
    MPI_Gatherv(subvector_c, recv_count, MPI_FLOAT, c, send_counts, displacement, MPI_FLOAT, 0, MPI_COMM_WORLD);

    // get the end time
    t2 = MPI_Wtime();

    // check the results in the root process
    if (rank == 0)
    {
        for (int i = 0; i < array_size; i++)
        {
            float val = c[i] - c_expected[i];
            val = val * val;

            if (val > TOL)
                error_count++;
        }

        double exec_time = t2 - t1;

        printf("vectors with size %d added with %d errors in %lf seconds\n", array_size, error_count, exec_time);

        free(a);
        free(b);
        free(c);
        free(c_expected);
    }

    free(subvector_a);
    free(subvector_b);
    free(subvector_c);

    free(send_counts);
    free(displacement);

    MPI_Finalize();
}